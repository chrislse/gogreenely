FROM golang

ENV GIN_MODE=release

WORKDIR /go/src/gogreenely
COPY . .

RUN apt update && \
      apt install sqlite3

RUN ./setup.sh

RUN go get -d -v ./...
RUN go install -v ./...

CMD ["gogreenely"]
