require 'csv'
require 'httparty'
require 'json'


class DataEndPoint
  include HTTParty

  base_uri 'localhost:8080'

  def initialize()
    @token = ''
  end

  def login(username, password)
    response = self.class.post('/api/auth/login', body: "body=#{{username: username, password: password}.to_json}")
    @token = response['Token']
    response
  end

  def get_data(start, resolution)
    self.class.get('/api/v1/data', headers: {'Token' => @token}, query: { start: start, count: 20, resolution: resolution })
  end
end

data_endpoint = DataEndPoint.new
data_endpoint.login('snowballshiloh', 'Th1sEspassword')

puts "Montly data"
puts data_endpoint.get_data('2014-09-02', 'M')
puts "--------------------"
puts "Daily data"
puts data_endpoint.get_data('2014-09-02', 'D')

