require 'csv'
require 'httparty'
require 'json'


class Limits
  include HTTParty

  base_uri 'localhost:8080'

  def initialize()
    @token = ''
  end

  def login(username, password)
    response = self.class.post('/api/auth/login', body: "body=#{{username: username, password: password}.to_json}")
    @token = response["Token"]
    response
  end

  def get_limits()
    self.class.get('/api/v1/limits', headers: {'Token' => @token} )
  end
end

limit_endpoint = Limits.new
limit_endpoint.login("snowballshiloh", "Th1sEspassword")
puts limit_endpoint.get_limits()

