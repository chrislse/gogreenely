#!/bin/sh

# prepare db with seeds
rm -rf greenely.db
cat table_creations.sql | sqlite3 greenely.db
cat users.sql | sqlite3 greenely.db
cat months.sql | sqlite3 greenely.db
cat days.sql | sqlite3 greenely.db

# prepare server side public/private key for jwt
openssl genrsa -out keys/app.rsa 1024
openssl rsa -in keys/app.rsa -pubout > keys/app.rsa.pub

