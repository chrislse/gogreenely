package main

import (
	"crypto/rsa"
	"github.com/gin-gonic/gin"
	"github.com/jinzhu/gorm"
	"io/ioutil"
	"net/http"

	jwt "github.com/dgrijalva/jwt-go"
	_ "github.com/jinzhu/gorm/dialects/sqlite"
)

var db *gorm.DB

const (
	privKeyPath = "keys/app.rsa"     // openssl genrsa -out app.rsa keysize
	pubKeyPath  = "keys/app.rsa.pub" // openssl rsa -in app.rsa -pubout > app.rsa.pub

	// Response Messages
	ResMsgLoginOk               = "login is ok"
	ResMsgLoginFailed           = "incorrect username or password"
	ResMsgLoginFailedWithToken  = "you have invalid auth token, please login again"
	ResMsgInvalidDataParameters = "start cannot be empty, resolution needs to be D or M"
)

var (
	verifyKey *rsa.PublicKey
	signKey   *rsa.PrivateKey
)

func main() {

	setupJwtKeys()
	//preparation ends

	db, err := gorm.Open("sqlite3", "greenely.db")
	checkErr(err)
	db.LogMode(false)

	defer db.Close()

	router := gin.Default()

	auth := router.Group("/api/auth/")
	{
		auth.POST("/login", Login)
	}

	v1 := router.Group("/api/v1/")
	v1.Use(TokenAuth())
	{
		v1.POST("/user", CreateUser)
		v1.GET("/user", GetUser)
		v1.PUT("/user", UpdateUser)
		v1.DELETE("/user", DeleteUser)
		v1.GET("/limits", GetLimits)
		v1.GET("/data", GetData)
	}
	router.Run()

}

func setupJwtKeys() {
	signBytes, err := ioutil.ReadFile(privKeyPath)
	checkErr(err)

	signKey, err = jwt.ParseRSAPrivateKeyFromPEM(signBytes)
	checkErr(err)

	verifyBytes, err := ioutil.ReadFile(pubKeyPath)
	checkErr(err)

	verifyKey, err = jwt.ParseRSAPublicKeyFromPEM(verifyBytes)
	checkErr(err)
}

func TokenAuth() gin.HandlerFunc {
	return func(c *gin.Context) {
		v := GetHeader(c, "Token")
		if v == "" {
			response := LoginResponse{Message: ResMsgLoginFailedWithToken}
			c.AbortWithStatusJSON(http.StatusUnauthorized, response)
		} else {

			token, err := jwt.Parse(v, func(token *jwt.Token) (interface{}, error) {
				// since we only use the one private key to sign the tokens,
				// we also only use its public counter part to verify
				return verifyKey, nil
			})

			if err != nil {
				response := LoginResponse{Message: ResMsgLoginFailedWithToken}
				c.AbortWithStatusJSON(http.StatusUnauthorized, response)
			} else {
				claims := token.Claims.(jwt.MapClaims)
				username := GetUsernameFromTokenClaims(claims)
				c.Set("username", username)
			}
		}

	}
}

func GetHeader(c *gin.Context, name string) string {
	if values, _ := c.Request.Header[name]; len(values) > 0 {
		return values[0]
	}
	return ""
}

func GetUsernameFromTokenClaims(claims jwt.MapClaims) string {
	info := claims["CustomUserInfo"]
	return info.(map[string]interface{})["Name"].(string)
}

// end of limit file

func checkErr(err error) {
	if err != nil {
		panic(err)
	}
}
