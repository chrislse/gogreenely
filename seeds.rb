require 'csv'
require 'httparty'
require 'json'


class User
  include HTTParty

  base_uri 'localhost:8080'

  def create(user)
    self.class.post('/api/v1/user', body: user)
  end
end

user_endpoint = User.new

CSV.foreach('usernames.csv', col_sep: ',') do |row|
  user_credential = {username: row[0], password: row[1]}
  user_endpoint.create("body=#{user_credential.to_json}")
end

