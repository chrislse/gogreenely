package main

import (
	"github.com/gin-gonic/gin"
	"github.com/jinzhu/gorm"
	"net/http"
	"strconv"
)

type DataResponse struct {
	Data []interface{} `json:"data"`
}
type Day struct {
	UserId      int
	Timestamp   string
	Consumption int
	Temperature int
}

type Month struct {
	UserId      int
	Timestamp   string
	Consumption int
	Temperature int
}

func GetData(c *gin.Context) {
	start := c.Query("start")

	count, err := strconv.Atoi(c.DefaultQuery("count", "10"))
	checkErr(err)

	resolution := c.Query("resolution")

	if start == "" || (resolution != "D" && resolution != "M") {
		c.JSON(http.StatusBadRequest, LoginResponse{Message: ResMsgInvalidDataParameters})
	} else {
		username, _ := c.Get("username")
		db, err := gorm.Open("sqlite3", "greenely.db")
		defer db.Close()

		user := User{}
		err = db.Where("username = ?", username).Limit(1).Find(&user).Error
		checkErr(err)

		data := []interface{}{}
		if resolution == "D" {
			days := []Day{}
			GetDaysDataForUser(db, user.ID, start, count, &days)

			for _, day := range days {
				data = append(data, ConvertDayToInterfaceList(day))
			}

		} else if resolution == "M" {
			months := []Month{}
			GetMonthsDataForUser(db, user.ID, start, count, &months)
			for _, month := range months {
				data = append(data, ConvertMonthToInterfaceList(month))
			}
		}

		response := DataResponse{Data: data}

		c.JSON(http.StatusOK, response)
	}
}

func GetDaysDataForUser(db *gorm.DB, userId uint, date string, count int, days *[]Day) {
	err := db.Where("user_id = ? and timestamp >= ?", userId, date).Order("timestamp ASC").Limit(count).Find(&days).Error
	checkErr(err)

}

func GetMonthsDataForUser(db *gorm.DB, userId uint, date string, count int, months *[]Month) {
	err := db.Where("user_id = ? and timestamp >= ?", userId, date).Order("timestamp ASC").Limit(count).Find(&months).Error
	checkErr(err)

}

func ConvertDayToInterfaceList(day Day) []interface{} {
	return []interface{}{getDateOf(day.Timestamp), day.Consumption, day.Temperature}
}

func ConvertMonthToInterfaceList(month Month) []interface{} {
	return []interface{}{getDateOf(month.Timestamp), month.Consumption, month.Temperature}
}
