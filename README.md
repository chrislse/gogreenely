a golang impl of greenely, it can be ported into python later

# How to start the project

## Requirements:
* Internet
* Linux
* openssl / sqlite3
* Docker
* make
* ruby(for testing)

In order to run the app in docker, you just type
```
make run
```
It will build a local docker image for you and run the app from the container

## setup.sh
The bash script does two things
* seeds the db model into sqlite db
* generate token rsa keys in keys folder by using openssl

# How to test it.

After the app is running. It can be tested by

```
ruby limits.rb

ruby data.rb
```

## How test script works
The ruby scripts try to login as a normal user and then do http request to the endpoints, with given auth token.

A list of usernames can be found in usernames.txt. All users share the same passowrd:
```
Th1sEspassword
```

# Model Diagram


      +--------------------+       +---------------------+
      | users              |       | days                |
      |                    |       +---------------------+
      +--------------------+ ref   | day_id     pk       |
      | id    pk           <-+-----+ user_id             |
      | created_at         | |     | timestamp           |
      | updated_at         | |     | consumption         |
      | deleted_at         | |     | temperature         |
      | username           | |     |                     |
      | password_salt      | |     +---------------------+
      | password_hash      | |
      |                    | |     +---------------------+
      |                    | |     | months              |
      +--------------------+ |     +---------------------+
                             |     | month_id    pk      |
                             +-----+ user_id             |
                                   | timestamp           |
                             ref   | consumption         |
                                   | temperature         |
                                   |                     |
                                   +---------------------+

- [x] use salt to store passward of user
- [x] seed some testing user
- [x] use proper http return code to represent the login result
- [x] separate db and user into different files as receiver
- [x] use token authentication "github.com/dgrijalva/jwt-go"
- [x] complete the data flow
- [x] add a dockerfile
- [ ] add a pipeline to deploy to aws
- [ ] try to open less db connections
- [ ] add request ip into token customer user info

