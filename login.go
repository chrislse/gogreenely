package main

import (
	"encoding/json"
	"github.com/gin-gonic/gin"
	"github.com/jinzhu/gorm"
	"net/http"
	"time"

	jwt "github.com/dgrijalva/jwt-go"
)

type LoginInfo struct {
	Username string `json:"username"` // it can be email as well
	Password string `json:"password"`
}

type LoginResponse struct {
	Token   string
	Message string
}

func Login(c *gin.Context) {
	info := LoginInfo{}
	post := c.PostForm("body")
	json.Unmarshal([]byte(post), &info)

	var user User
	db, err := gorm.Open("sqlite3", "greenely.db")
	checkErr(err)
	db.First(&user, "username = ?", info.Username)
	// return error when res is 0

	hash := EncryptedPassword(info.Password, user.PasswordSalt)

	// return error when User.PasswordHash != hash
	if hash == user.PasswordHash {
		response := LoginResponse{Token: GenerateToken(info.Username), Message: ResMsgLoginOk}

		c.JSON(http.StatusOK, response)
	} else {
		response := LoginResponse{Message: ResMsgLoginFailed}
		c.JSON(http.StatusUnauthorized, response)
	}
}

func GenerateToken(username string) string {
	t := jwt.New(jwt.GetSigningMethod("RS256"))
	// set our claims
	claims := t.Claims.(jwt.MapClaims)
	claims["AccessToken"] = "level1"
	claims["CustomUserInfo"] = struct {
		Name string
	}{username}

	claims["exp"] = time.Now().Add(time.Minute * 5).Unix()

	tokenString, err := t.SignedString(signKey)

	checkErr(err)

	return tokenString
}
