package main

import (
	"crypto/rand"
	"encoding/json"
	"github.com/gin-gonic/gin"
	"github.com/jinzhu/gorm"
	"golang.org/x/crypto/scrypt"
	"io"
	"net/http"
)

const (
	PW_SALT_BYTES = 32
	PW_HASH_BYTES = 64
)

type User struct {
	gorm.Model
	Username     string
	PasswordSalt string
	PasswordHash string
}

func CreateUser(c *gin.Context) {
	post := c.PostForm("body")
	userCredential := LoginInfo{}
	json.Unmarshal([]byte(post), &userCredential)

	db, err := gorm.Open("sqlite3", "greenely.db")
	defer db.Close()
	checkErr(err)
	StorePassword(db, userCredential)
	c.JSON(http.StatusOK, gin.H{"status": "user created"})
}

func GetUser(c *gin.Context)    {}
func UpdateUser(c *gin.Context) {}
func DeleteUser(c *gin.Context) {}

func StorePassword(db *gorm.DB, info LoginInfo) {

	salt := make([]byte, PW_SALT_BYTES)
	_, err := io.ReadFull(rand.Reader, salt)

	checkErr(err)

	hash := EncryptedPassword(info.Password, string(salt))

	saveUser(db, info.Username, string(salt), hash)
}

func EncryptedPassword(password string, salt string) string {
	hash, err := scrypt.Key([]byte(password), []byte(salt), 1<<14, 8, 1, PW_HASH_BYTES)
	checkErr(err)
	return string(hash)
}
func saveUser(db *gorm.DB, username, salt, hash string) {
	db.Create(&User{Username: username, PasswordSalt: salt, PasswordHash: hash})
}
