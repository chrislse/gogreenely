.PHONY: docker run

docker:
	docker build . -t gogreenely

run: docker
	docker run -p 8080:8080 gogreenely
