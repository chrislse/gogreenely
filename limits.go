package main

import (
	"github.com/gin-gonic/gin"
	"github.com/jinzhu/gorm"
	"net/http"
	"strings"
)

type RangeLimitStringType struct {
	Minimum string `json:"minimum"`
	Maximum string `json:"maximum"`
}

type RangeLimitNumberType struct {
	Minimum int `json:"minimum"`
	Maximum int `json:"maximum"`
}

type LimitsCollection struct {
	Timestamp   RangeLimitStringType `json:"timestamp"`
	Consumption RangeLimitNumberType `json:"consumption"`
	Temperature RangeLimitNumberType `json:"temperature"`
}

type LimitsType struct {
	Months LimitsCollection `json:"months"`
	Days   LimitsCollection `json:"days"`
}

type LimitsResponse struct {
	Limits LimitsType `json:"limits"`
}

func GetMonthsLimitsFromDb(username string) LimitsCollection {
	db, err := gorm.Open("sqlite3", "greenely.db")
	defer db.Close()

	user := User{}
	err = db.Where("username = ?", username).Limit(1).Find(&user).Error
	checkErr(err)

	maxMonth := Month{}
	minMonth := Month{}
	err = db.Where("user_id = ?", user.ID).Order("temperature DESC").Limit(1).Find(&maxMonth).Error
	checkErr(err)
	err = db.Where("user_id = ?", user.ID).Order("temperature ASC").Limit(1).Find(&minMonth).Error
	checkErr(err)

	return LimitsCollection{Timestamp: RangeLimitStringType{Maximum: getDateOf(maxMonth.Timestamp), Minimum: getDateOf(minMonth.Timestamp)},
		Consumption: RangeLimitNumberType{Maximum: maxMonth.Consumption, Minimum: minMonth.Consumption},
		Temperature: RangeLimitNumberType{Maximum: maxMonth.Temperature, Minimum: minMonth.Temperature},
	}
}

func GetDaysLimitsFromDb(username string) LimitsCollection {
	db, err := gorm.Open("sqlite3", "greenely.db")
	defer db.Close()

	user := User{}
	err = db.Where("username = ?", username).Limit(1).Find(&user).Error
	checkErr(err)

	maxDay := Day{}
	minDay := Day{}
	err = db.Where("user_id = ?", user.ID).Order("temperature DESC").Limit(1).Find(&maxDay).Error
	checkErr(err)
	err = db.Where("user_id = ?", user.ID).Order("temperature ASC").Limit(1).Find(&minDay).Error
	checkErr(err)

	return LimitsCollection{Timestamp: RangeLimitStringType{Maximum: getDateOf(maxDay.Timestamp), Minimum: getDateOf(minDay.Timestamp)},
		Consumption: RangeLimitNumberType{Maximum: maxDay.Consumption, Minimum: minDay.Consumption},
		Temperature: RangeLimitNumberType{Maximum: maxDay.Temperature, Minimum: minDay.Temperature},
	}
}

func GetLimits(c *gin.Context) {
	username, _ := c.Get("username")
	monthsLimits := GetMonthsLimitsFromDb(username.(string))
	daysLimits := GetDaysLimitsFromDb(username.(string))

	limits := LimitsType{Months: monthsLimits, Days: daysLimits}
	response := LimitsResponse{Limits: limits}

	c.JSON(http.StatusOK, response)
}

func getDateOf(timestamp string) string {
	return (strings.Split(timestamp, " "))[0]
}
